//
//  main.m
//  直播开发-RTMP-1-采集摄像头的视频图像
//
//  Created by mac on 16/5/11.
//  Copyright © 2016年 guen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
