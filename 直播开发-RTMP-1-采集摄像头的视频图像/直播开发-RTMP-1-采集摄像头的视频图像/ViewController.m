//
//  ViewController.m
//  直播开发-RTMP-1-采集摄像头的视频图像
//
//  Created by mac on 16/5/11.
//  Copyright © 2016年 guen. All rights reserved.
//

#import "ViewController.h"
//#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController () <AVCaptureVideoDataOutputSampleBufferDelegate>{
    dispatch_queue_t _outputQueue;
}
/**会话*/
@property(nonatomic, strong) AVCaptureSession *session;
/**采集输出流对象*/
@property(nonatomic, strong) AVCaptureVideoDataOutput *videoOutput;
/**采集连接对象,用于判断是何种类型数据*/
@property(nonatomic, weak) AVCaptureConnection  *videoConnection;
/**播放采集到的视频的图层*/
@property(nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreview;

@end

@implementation ViewController

#pragma - mark 懒加载
-(AVCaptureSession *)session{
    if (_session == nil) {
        _session = [[AVCaptureSession alloc] init];
    }
    return _session;
}
-(AVCaptureVideoDataOutput *)videoOutput{
    if (_videoOutput == nil) {
        _outputQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
        _videoOutput = [[AVCaptureVideoDataOutput alloc] init];
        [_videoOutput setSampleBufferDelegate:self queue:_outputQueue];
    }
    return _videoOutput;
}
-(AVCaptureVideoPreviewLayer *)videoPreview{
    if (_videoPreview == nil) {
        _videoPreview = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
        //设置预览时的视频缩放格式
        self.videoPreview.videoGravity = AVLayerVideoGravityResizeAspectFill;
        //设置视频的朝向
        [[self.videoPreview connection] setVideoOrientation:AVCaptureVideoOrientationPortrait];
        //设置播放器大小
        self.videoPreview.frame = self.view.layer.bounds;
    }
    return _videoPreview;
}

#pragma - mark 系统方法
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAVOutput];
    [self.session startRunning];
    //
    [self.view.layer addSublayer:self.videoPreview];
}
#pragma - mark 私有方法
/** 设置采集视频的输入输出对象 */
-(void)setAVOutput{
    //1获得一个采集设备对象-> 比如摄像头
    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    //2用设备初始化一个采集的输入对象
    NSError *error = nil;
    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
    if (error) {
        NSLog(@"%@",error);
    }
    if ([self.session canAddInput:videoInput]) {
        [self.session addInput:videoInput];//让采集会话管理采集输入对象
    }
    //3配置采集的输出对象, 即获得视频图像的接口
    /**
    _outputQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
    _output = [[AVCaptureVideoDataOutput alloc] init];
    [_output setSampleBufferDelegate:self queue:_outputQueue];
     */
    //4配置输出视频图像格式
    NSDictionary *captureSettings = @{(NSString*)kCVPixelBufferPixelFormatTypeKey: @(kCVPixelFormatType_32BGRA)};
    
    self.videoOutput.videoSettings = captureSettings;
    
    self.videoOutput.alwaysDiscardsLateVideoFrames = YES;
    
    if ([self.session canAddOutput:self.videoOutput]) {
        [self.session addOutput:self.videoOutput];
    }
    
    //5保存connection, 用于在代理中判断是audio还是video
    self.videoConnection = [self.videoOutput connectionWithMediaType:AVMediaTypeVideo];
}


#pragma - mark AVCaptureVideoDataOutputSampleBufferDelegate 输出流代理 用于获取采集到的视频
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if (connection == self.videoConnection) {
        /*
         // 取得当前视频尺寸信息
         CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
         int width = CVPixelBufferGetWidth(pixelBuffer);
         int height = CVPixelBufferGetHeight(pixelBuffer);
         NSLog(@"video width: %d  height: %d", width, height);
         */
        NSLog(@"在这里获得video sampleBuffer，做进一步处理（编码H.264）");
    }
//    else if (connection == _audioConnection) {  // Audio
//        NSLog(@"这里获得audio sampleBuffer，做进一步处理（编码AAC）");
//    }
}

@end
